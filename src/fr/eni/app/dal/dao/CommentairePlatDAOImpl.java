package fr.eni.app.dal.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.eni.app.dal.PoolConnection;
import fr.eni.app.model.bll.bo.Commentaire;
import fr.eni.app.model.bll.bo.Plat;

public class CommentairePlatDAOImpl implements CommentairePlatDAO{
	public static CommentairePlatDAO instance = null;
	
	public CommentairePlatDAOImpl() {
		// TODO Auto-generated constructor stub
	}
	
	public static CommentairePlatDAO getInstance() {
		if (instance == null) {
			instance = new CommentairePlatDAOImpl();
		}
		return instance;
	}
	
	
	@Override
	public boolean ajouteCommPlat(Commentaire comm) {
		boolean ok = false;
		Connection cnx = PoolConnection.getConnection();
		try {
			PreparedStatement query = cnx.prepareStatement("INSERT INTO Commentaire_Plat (IDUtilisateur, IDPlat, contenuCommentaire, note) VALUES (?,?,?,?);");
			query.setInt(1, comm.getIdUtilisateur());
			query.setInt(2, comm.getIdPlat());
			query.setString(3, comm.getCommentaire());
			query.setFloat(4, comm.getNote());
			System.out.println(comm.getIdUtilisateur() + " " + comm.getIdPlat() + " " + comm.getCommentaire() + " " + comm.getNote());
			System.out.println("4" + query);
			query.executeUpdate();
			ok = true;
			
		}catch(SQLException e) {System.out.println(e.toString());}
		return ok;
	}

	@Override
	public boolean supprimeCommPlat(Commentaire comm) {
		boolean ok = false;
		Connection cnx = PoolConnection.getConnection();
		try {
			PreparedStatement query = cnx.prepareStatement("DELETE FROM Commentaire_Plat WHERE ID=?;");
			query.setInt(1,  comm.getId());
			
			query.executeUpdate();
			cnx.close();
		}catch(SQLException e) {System.out.println(e.toString());}
		return ok;
	}

	@Override
	public Commentaire modifieCommPlat(Commentaire comm) {
		Connection cnx = PoolConnection.getConnection();
		try {
			PreparedStatement query = cnx.prepareStatement("UPDATE Commentaire_Plat SET contenuCommentaire = ?, note = ?) WHERE id=?");
			query.setString(1, comm.getCommentaire());
			query.setFloat(2, comm.getNote());
			query.setInt(3, comm.getId());
			
			query.executeUpdate();
			
			cnx.close();
		}catch(SQLException e) {e.toString();}
		return comm;	
	}

	@Override
	public List<Commentaire> afficheCommPlat(Plat plat) {
		List<Commentaire> comms = new ArrayList<Commentaire>();
		Connection cnx = PoolConnection.getConnection();
		try {
			Commentaire comm = new Commentaire();
			PreparedStatement pstmt = cnx.prepareStatement("SELECT * FROM Commentaire WHERE IDPlat=?");
			pstmt.setInt(1, plat.getId());
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				comm.setId(rs.getInt("ID"));
				comm.setIdUtilisateur(rs.getInt("IDUtilisateur"));
				comm.setIdPlat(plat.getId());
				comm.setCommentaire(rs.getString("contenuCommentaire"));
				comm.setNote(rs.getFloat("note"));
				
				comms.add(comm);
			}
			System.out.println(comms);
			cnx.close();
		}catch(SQLException e) {System.out.println(e.toString());}
		return comms;
	}
}
