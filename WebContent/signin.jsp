<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %>
<%@include file="include/head.jsp"%>
<%@include file="include/navbar.jsp"%>

<div class="d-flex all_page">
	<div class="m-auto">
		<h2 class="mb-3">
			Connexion
		</h2>
		<form action="signin/ask" method="POST">
			<div class="container signup_container">
				<div class="row">
					<div class="col-auto input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text input_border_radius_left">
								<i class="fas fa-at"></i>
							</span>
						</div>
						<input type="email" class="form-control input_border_radius_right" name="mail" placeholder="Courriel" value="${ utilisateur.mail }" required>
					</div>
					<div class="col-auto input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text input_border_radius_left">
								<i class="fas fa-key"></i>
							</span>
						</div>
						<input type="password" class="form-control input_border_radius_right" name="mdp" placeholder="Mot de passe" required>
					</div>
				</div>
			</div>
			<button type="submit"
				class="btn btn-success font-weight-bold submit_border_radius px-4 mt-3">
				<i class="fas fa-lock-alt"></i> Se connecter
			</button>
		</form>
	</div>
</div>

<script>
	activeNav('signin');
</script>

<%@include file="include/foot.jsp"%>