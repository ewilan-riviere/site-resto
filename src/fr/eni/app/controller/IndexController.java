package fr.eni.app.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import fr.eni.app.model.bll.CommentairePlatManager;
import fr.eni.app.model.bll.CommentairePlatManagerImpl;
import fr.eni.app.model.bll.IdentificationManager;
import fr.eni.app.model.bll.IdentificationManagerImpl;
import fr.eni.app.model.bll.PlatManager;
import fr.eni.app.model.bll.PlatManagerImpl;
import fr.eni.app.model.bll.bo.Navbar;
import fr.eni.app.model.bll.bo.Plat;
import fr.eni.app.model.bll.bo.Utilisateur;

/**
 * Servlet implementation class IndexController
 */
@WebServlet("/sr/*")
@MultipartConfig
public class IndexController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private IdentificationManager identificationManager = IdentificationManagerImpl.getInstance();
	private PlatManager platManager = PlatManagerImpl.getInstance();
	private CommentairePlatManager commentairePlatManager = CommentairePlatManagerImpl.getInstance();
	
//	Connected User
	private boolean isConnected = false;
	private Utilisateur utilisateur;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Main Controller");
//		Apparait tout le temps 
		List<Navbar> NavbarMain = new ArrayList<>();
		NavbarMain.add(new Navbar("home", "sr/home", "fas fa-home"));
		NavbarMain.add(new Navbar("plats", "sr/plats", "fas fa-utensils"));
//		Apparait si l'utilisateur n'est pas connect� 
		List<Navbar> navbarNoConnected = new ArrayList<>();
			navbarNoConnected.add(new Navbar("signin", "sr/signin", "fas fa-sign-in-alt"));
			navbarNoConnected.add(new Navbar("signup", "sr/signup", "fas fa-user"));
//		Apprait si l'utilisateur est connect� 
		List<Navbar> navbarConnected = new ArrayList<>();
			navbarConnected.add(new Navbar("signout", "sr/signout", "fas fa-sign-out-alt"));
			navbarConnected.add(new Navbar("account", "sr/account", "fas fa-user"));
			navbarConnected.add(new Navbar("settings", "settings", "fas fa-cog"));
		
		HttpSession session = request.getSession();
		
		session.setAttribute("NavbarMain", NavbarMain);
		session.setAttribute("navbarNoConnected", navbarNoConnected);
		session.setAttribute("navbarConnected", navbarConnected);
		session.setAttribute("isConnected", isConnected);
		
		String action = request.getRequestURI();
		switch (action) {
//		Page des plats
		case "/SiteResto/sr/plats":
//			List<Plat> plats = new ArrayList<>();
//				plats.add(new Plat("burger", "Sandwich", "assets/images/plats/burger_avocat_bacon.jpg", TypePlat.Plat, 0, 10, "Sandwich"));
//				plats.add(new Plat("cake", "dessert", "assets/images/plats/cake_banane_chocolat.jpg", TypePlat.Plat, 0, 10, " dessert"));
//				plats.add(new Plat("morue", "plat", "assets/images/plats/chiktaye_morue_guadeloupe.jpg", TypePlat.Plat, 0, 10, " plat"));
//				plats.add(new Plat("salade", "entr�e", "assets/images/plats/salade_automne_champignons_sautes.jpg", TypePlat.Plat, 0, 10, " entr�e"));
//				plats.add(new Plat("salade", "entr�e", "assets/images/plats/salade_chevre_chaud.jpg", TypePlat.Plat, 0, 10, " entr�e"));
//				plats.add(new Plat("taboule", "plat", "assets/images/plats/taboule_libanais.jpg", TypePlat.Plat, 0, 10, " plat"));
//				plats.add(new Plat("tarte", "dessert", "assets/images/plats/tarte_tatin.jpg", TypePlat.Plat, 0, 10, " dessert"));
			List<Plat> plats = platManager.selectPlats();
			session.setAttribute("plats", plats);
			this.getServletContext().getRequestDispatcher("/nosPlats.jsp").forward(request, response);
			break;
			// La page Mon Compte
		case "/SiteResto/sr/account":
			this.getServletContext().getRequestDispatcher("/MonCompte.jsp").forward(request, response);
			break;
		case "/SiteResto/sr/account/ask":
			updateUtilisateur(request,response);
			this.getServletContext().getRequestDispatcher("/MonCompte.jsp").forward(request, response);
			break;
//		Ajout d'un plat 
		case "/SiteResto/sr/plats/add-plat":
			this.getServletContext().getRequestDispatcher("/AjoutUnPlat.jsp").forward(request, response);
			break;
		case "/SiteResto/sr/plats/add-plat/ask":
			addPlat(request,response);
			this.getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
			break;
//		Ajout d'un commentaire sur le plat
		case "/SiteResto/sr/plats/comment":
			newCommPlat(request,response);
			this.getServletContext().getRequestDispatcher("/nosPlats.jsp").forward(request, response);
			break;
//		Connexion d'un utilisateur 
		case "/SiteResto/sr/signin":
			this.getServletContext().getRequestDispatcher("/signin.jsp").forward(request, response);
			break;
		case "/SiteResto/sr/signout":
			signOutUser(request,response);
			Integer compteurConnecte = (Integer) getServletContext().getAttribute("CompteurConnecte");
			if (compteurConnecte != null) {
				getServletContext().setAttribute("CompteurConnecte", compteurConnecte--);
			} else {
				getServletContext().setAttribute("CompteurConnecte", 0);
			}
			this.getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
			break;
		case "/SiteResto/sr/signin/ask":
			System.out.println("signin/ask");
			isConnected = signInUser(request,response);
			session.setAttribute("isConnected", isConnected);
			if (isConnected == true) {
				compteurConnecte = (Integer) getServletContext().getAttribute("CompteurConnecte");
				if (compteurConnecte != null) {
					getServletContext().setAttribute("CompteurConnecte", compteurConnecte++);
				} else {
					getServletContext().setAttribute("CompteurConnecte", 1);
				}
				this.getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
			}
			else {
				this.getServletContext().getRequestDispatcher("/signin.jsp").forward(request, response);
			}
			System.out.println("fdjogdjkhjdflk");
			break;
			
//			Enregistrer un nouvel utilisateur 
		case "/SiteResto/sr/signup":
//			Redirection vers la page d'inscription 
			this.getServletContext().getRequestDispatcher("/signup.jsp").forward(request, response);
			break;
		case "/SiteResto/sr/signup/ask":
//			Envoi d'informations depuis le formulaire
			System.out.println("signup/ask");
			signUpUser(request,response);
			break;

		default:
			System.out.println(getServletContext().getAttribute("CompteurConnecte"));
			this.getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
			break;
		}
	}
	
	private void updateUtilisateur(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		Utilisateur utilisateur = (Utilisateur)session.getAttribute("utilisateur");
		int id = utilisateur.getId();
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String mdp = request.getParameter("mdp");
		String mail = request.getParameter("mail");
		String decouverte = request.getParameter("decouverte");
		String avatar = request.getParameter("avatar");
		boolean statut = utilisateur.isStatut();
		
		Utilisateur utilisateurUpdate = identificationManager.updateUtilisateur(id,nom,prenom,mdp,mail,decouverte,avatar,statut );
	}
	
	private void signOutUser(HttpServletRequest request, HttpServletResponse response) {
		isConnected = false;
		HttpSession session = request.getSession();
		String utilisateur = null;
		session.setAttribute("utilisateur",utilisateur);
		session.setAttribute("isConnected", isConnected);
	}

	private void newCommPlat(HttpServletRequest request, HttpServletResponse response) {
		commentairePlatManager.newCommPlat(1, 1, "terrib", 5);
	}

	private void addPlat(HttpServletRequest request, HttpServletResponse response) throws IOException, IllegalStateException, ServletException {
		final String UPLOAD_DIR = "plats";
		
		String nom = request.getParameter("nom");
		String descr = request.getParameter("descr");
		String photo = request.getParameter("photo");
		String type = request.getParameter("type");
		String prix = request.getParameter("prix");
		String recette = request.getParameter("recette");
		
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		// gets absolute path of the web application
		String applicationPath = "C:\\Users\\Administrateur\\eclipse-workspace\\SiteResto\\WebContent\\assets\\images\\";
		System.out.println(applicationPath);
		// constructs path of the directory to save uploaded file
		String uploadFilePath = applicationPath + UPLOAD_DIR;
		// creates upload folder if it does not exists
		File uploadFolder = new File(uploadFilePath);
		if (!uploadFolder.exists()) {
			uploadFolder.mkdirs();
		}
		PrintWriter writer = response.getWriter();
		// write all files in upload folder
		Part part = request.getPart("avatar");
		String fileName = getSubmittedFileName(part);
		String contentType = part.getContentType();
		
		// allows only JPEG files to be uploaded
//				if (!contentType.equalsIgnoreCase("image/jpeg") || !contentType.equalsIgnoreCase("image/jpg") || !contentType.equalsIgnoreCase("image/png")) {
//					continue;
//				}
		
		part.write(uploadFilePath + File.separator + nom+"_"+fileName);
		if (!photo.isEmpty()) {
			photo = nom+"_"+fileName;
		}
		
		Plat plat = platManager.addPlat(nom,descr,photo,type,prix,recette);
		String validation = "Plat ajout�";
		request.setAttribute("validation", validation);
	}

	private boolean signInUser(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("signInUser");
		String mail = request.getParameter("mail");
		String mdp = request.getParameter("mdp");
		boolean isConnected = false;
		Utilisateur utilisateur = identificationManager.getUser(mail,mdp);
//		User SignIn
		if (utilisateur.getId() > 0) {
//			if (keepit != null) {
//				LocalDateTime now = LocalDateTime.now();
//				String nowString = now.toString();
//				prepareCookie(response, nowString, 60*60*24*365);
//			} else {
//				prepareCookie(response, "", 0);
//			}
			isConnected = true;
			HttpSession session = request.getSession();
			session.setAttribute("utilisateur",utilisateur);
		} else {
			request.setAttribute("errConnection", "E-mail ou mot de passe incorrect");
		}
		return isConnected;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
//	Enregistrer un nouvel utilisateur 
	private void signUpUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String UPLOAD_DIR = "avatars";
		
		String mail = request.getParameter("mail");
		String prenom = request.getParameter("prenom");
		String nom = request.getParameter("nom");
		String avatar = request.getParameter("avatar");
		String mdp = request.getParameter("mdp");
		String mdpConfirm = request.getParameter("mdp-confirm");
		String decouverte = request.getParameter("decouverte");
		
		if (avatar == null) avatar = "default";
		if (decouverte == "") decouverte = "default";
		
//		Part filePart = request.getPart("avatar");
//		String fileName = getSubmittedFileName(filePart);
		
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		// gets absolute path of the web application
		String applicationPath = "C:\\Users\\Administrateur\\eclipse-workspace\\SiteResto\\WebContent\\assets\\images\\";
		System.out.println(applicationPath);
		// constructs path of the directory to save uploaded file
		String uploadFilePath = applicationPath + UPLOAD_DIR;
		// creates upload folder if it does not exists
		File uploadFolder = new File(uploadFilePath);
		if (!uploadFolder.exists()) {
			uploadFolder.mkdirs();
		}
		PrintWriter writer = response.getWriter();
		// write all files in upload folder
		Part part = request.getPart("avatar");
		String fileName = getSubmittedFileName(part);
		String contentType = part.getContentType();
		
		// allows only JPEG files to be uploaded
//				if (!contentType.equalsIgnoreCase("image/jpeg") || !contentType.equalsIgnoreCase("image/jpg") || !contentType.equalsIgnoreCase("image/png")) {
//					continue;
//				}
		
		part.write(uploadFilePath + File.separator + mail+"_"+fileName);
		if (!avatar.isEmpty()) {
			avatar = mail+"_"+fileName;
		}

		Map<String, String> errors = identificationManager.checkSignUp(mdp, mdpConfirm);
		errors.forEach((key, value) -> System.out.println(key + ":" + value));
		
		if(errors.isEmpty()) {
			Utilisateur utilisateur = identificationManager.validSignup(mail, prenom, nom, mdp, decouverte, avatar);
			request.setAttribute("utilisateur", utilisateur);
			this.getServletContext().getRequestDispatcher("/signin.jsp").forward(request, response);
		} else {
			request.setAttribute("errors", errors);
			this.getServletContext().getRequestDispatcher("/signup.jsp").forward(request, response);
		}
		
		
	}
	
	private static String getSubmittedFileName(Part part) {
	    for (String cd : part.getHeader("content-disposition").split(";")) {
	        if (cd.trim().startsWith("filename")) {
	            String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
	            return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1); // MSIE fix.
	        }
	    }
	    return null;
	}

}
