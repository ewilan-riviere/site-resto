package fr.eni.app.dal.dao;

import java.util.List;

import fr.eni.app.model.bll.bo.Ingredient;
import fr.eni.app.model.bll.bo.Plat;
import fr.eni.app.model.bll.bo.Recette;

public interface PlatDAO {
	public boolean ajoutePlat(Plat plat);
	public void supprimePlat(Plat plat);
	public Plat modifiePlat(Plat plat);
	public List<Plat> affichePlat();
}
