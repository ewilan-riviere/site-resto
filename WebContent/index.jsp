<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %>
<%@include file="include/head.jsp"%>
<%@include file="include/navbar.jsp"%>

<div class="d-flex all_page">
	<div class="m-auto">
		<fmt:message key="index.string.welcome" bundle="${ string }"/>
		<h1 class="m-auto">
			<span class="navbar_title">
				<fmt:message key="head.title.title" bundle="${ title }"/>
			</span>
		</h1>
		<div>
			${ name }
			${ utilisateur.prenom } ${ utilisateur.nom }
		</div>
		<img src="${ pageContext.request.contextPath }/assets/images/logo.png" alt="logo">
	</div>
</div>

<script>
	activeNav('home');
</script>

<%@include file="include/footer.jsp"%>
<%@include file="include/foot.jsp"%>