<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@include file="include/head.jsp"%>
<%@include file="include/navbar.jsp"%>

<div class="bg-success text-white">${ validation }</div>
<form action="add-plat/ask" method="POST">
	<input type="text" name="nom" placeholder="Nom" required>
	<br>
	<input type="text" name="descr" placeholder="Descriptif">
	<br>
	<input type="file" name="photo">
	<br>
	<select name="type" required>
		<option selected disabled hidden>Type de plat</option>
		<option value="Entree">Entr�e</option>
		<option value="Plat">Plat de r�sistance</option>
		<option value="Dessert">Dessert</option>
	</select>
	<br>
	<input type="number" name="prix" placeholder="prix">
	<br>
	<input type="text" name="recette" placeholder="recette">
	<br>
	<button type="submit"
		class="btn btn-success font-weight-bold submit_border_radius px-4 mt-3">
		<i class="fas fa-plus-alt"></i> Ajouter
	</button>
</form>

<script>
	activeNav('plats');
</script>

<%@include file="include/foot.jsp"%>