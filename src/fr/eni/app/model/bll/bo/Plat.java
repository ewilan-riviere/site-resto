package fr.eni.app.model.bll.bo;

import java.util.List;
import java.io.Serializable;

public class Plat {
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String nom;
	private String descr;
	private String photo;
	private TypePlat type;
	private int compteur;
	private float prix;
	private String recette;
//	private List<Ingredient> ingredients;
	
	public Plat() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	public Plat(String nom, String descr, String photo, TypePlat type, int compteur, float prix,
			String recette) {
		super();
		this.nom = nom;
		this.descr = descr;
		this.photo = photo;
		this.type = type;
		this.compteur = compteur;
		this.prix = prix;
		this.recette = recette;
	}
	
	public Plat(int id, String nom, String descr, String photo, TypePlat type, int compteur, float prix,
			String recette) {
		super();
		this.id = id;
		this.nom = nom;
		this.descr = descr;
		this.photo = photo;
		this.type = type;
		this.compteur = compteur;
		this.prix = prix;
		this.recette = recette;
	}



	public String getRecette() {
		return recette;
	}

	public void setRecette(String recette) {
		this.recette = recette;
	}

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getDescr() {
		return descr;
	}



	public void setDescr(String descr) {
		this.descr = descr;
	}



	public String getPhoto() {
		return photo;
	}



	public void setPhoto(String photo) {
		this.photo = photo;
	}



	public TypePlat getTypePlat() {
		return type;
	}



	public void setTypePlat(TypePlat type) {
		this.type = type;
	}



	public int getCompteur() {
		return compteur;
	}



	public void setCompteur(int compteur) {
		this.compteur = compteur;
	}



	public float getPrix() {
		return prix;
	}



	public void setPrix(float prix) {
		this.prix = prix;
	}

	@Override
	public String toString() {
		return "Plat [id=" + id + ", nom=" + nom + ", descr=" + descr + ", photo=" + photo + ", type=" + type
				+ ", compteur=" + compteur + ", prix=" + prix + ", recette=" + recette + "]";
	}



	
	
	
	
	
	
	
	
	

}
