<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %>
<%@include file="include/head.jsp"%>
<%@include file="include/navbar.jsp"%>

<div class="${ utilisateur.statut == '1' ? '' : 'd-none' }">
	<a href="plats/add-plat">Ajouter un plat</a>
</div>

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
	<ol class="carousel-indicators">
	  	<c:forEach items="${ plats }" var="platsItems">
	    	<li data-target="#carouselExampleIndicators" class="${ platsItems.id == '1' ? 'active' : '' }"></li>
	    </c:forEach>
	  </ol>
	  
  	<c:forEach items="${ plats }" var="platsItems">
	    <div class="carousel-item ${ platsItems.id == '1' ? 'active' : '' }">
	      <img class="d-block w-100" src="${ pageContext.request.contextPath }/assets/images/plats//${ platsItems.photo }" alt="${ platsItems.nom }">
	    	<div class="carousel-caption d-none d-md-block">
			    <h5 class="text-white carousel_text font-weight-bold">
			    	${ platsItems.nom }
			    </h5>
			    <p class="text-white carousel_text font-weight-bold">
			    	${ platsItems.descr }
			    </p>
		  	</div>
	    </div>
    </c:forEach>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<ul>
<c:forEach items="${ plats }" var="platsItems">
	<li id="${ platsItems.nom }" class="nav-item">
		${ platsItems.nom }
	 	<ul>
	 		<li>
	 			Descriptif : ${ platsItems.descr }
	 		</li>
	 		<li>
	 			Prix : ${ platsItems.prix }€
	 		</li>
	 		<li>
	 			Recette : ${ platsItems.recette }
	 		</li>
	 		<li>
	 			Type : <%-- ${ platsItems.type } --%>
	 		</li>
	 	</ul>
	</li>
</c:forEach>
</ul>

<form action="plats/comment" method="POST">
<label> FAIRE UN COMMENTAIRE </label>
	<input type="number" value="5" class="form-control input_border_radius_right" name="note" required>	
	<textarea class="form-control input_border_radius_right" id="commPlat" name="commPlat" placeholder="commPlat"></textarea>
					
	<button type="submit" class="btn btn-success font-weight-bold submit_border_radius px-4 mt-3">POST</button>
</form>


<script>
	activeNav('plats');
</script>

<%@include file="include/foot.jsp"%>