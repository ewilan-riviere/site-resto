package fr.eni.app.model.bll.bo;

import java.time.LocalDateTime;

public class Commentaire {
	private int id;
	private int idUtilisateur;
	private int idPlat;
	private String commentaire;
	private float note;
	private LocalDateTime date_heure;
	


	public Commentaire(int idUtilisateur, int idPlat, String commentaire, float note, LocalDateTime date_heure) {
		super();
		this.idUtilisateur = idUtilisateur;
		this.idPlat = idPlat;
		this.commentaire = commentaire;
		this.note = note;
		this.date_heure = date_heure;
	}


	public Commentaire() {
		super();
	}


	public Commentaire(int id, int idUtilisateur, int idPlat, String commentaire, float note,
			LocalDateTime date_heure) {
		super();
		this.id = id;
		this.idUtilisateur = idUtilisateur;
		this.idPlat = idPlat;
		this.commentaire = commentaire;
		this.note = note;
		this.date_heure = date_heure;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getIdUtilisateur() {
		return idUtilisateur;
	}


	public void setIdUtilisateur(int idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}


	public int getIdPlat() {
		return idPlat;
	}


	public void setIdPlat(int idPlat) {
		this.idPlat = idPlat;
	}


	public String getCommentaire() {
		return commentaire;
	}


	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}


	public float getNote() {
		return note;
	}


	public void setNote(float note) {
		this.note = note;
	}


	@Override
	public String toString() {
		return "Commentaire [id=" + id + ", idUtilisateur=" + idUtilisateur + ", idPlat=" + idPlat + ", commentaire="
				+ commentaire + ", note=" + note + ", date_heure=" + date_heure + "]";
	}


	public LocalDateTime getDate_heure() {
		return date_heure;
	}


	public void setDate_heure(LocalDateTime date_heure) {
		this.date_heure = date_heure;
	}
	
	

}
