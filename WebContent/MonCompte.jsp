<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %>
<%@include file="include/head.jsp"%>
<%@include file="include/navbar.jsp"%>

<div class="d-flex all_page">
	<div class="m-auto">
		<h2 class="mb-3">
			MonCompte
		</h2>
		<form action="account/ask" method="POST">
			Nom :
			<br>
			<input name="nom" value="${ utilisateur.nom }">
			Prénom :
			<br>
			<input name="prenom" value="${ utilisateur.prenom }">
			<div class="container MonCompte_container">
				<div class="row">
					<div class="col-6 input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text input_border_radius_left">
								<i class="fas fa-user"></i>
							</span>
						
						<input type="password" class="form-control input_border_radius_right" name="mdp" placeholder=" changer le mot de passe" required>
					</div>
					<div class="col-6 input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text input_border_radius_left">
								<i class="fas fa-lock"></i>
							</span>
						</div>
						
						<input type="email" class="form-control input_border_radius_right" name="mail" placeholder="changer l’adresse mail" required>
					</div>
					<div class="col-6 input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text input_border_radius_left">
								<i class="fas fa-camera"></i>
							</span>
						</div>
						<div class="custom-file">
							<input type="file" class="custom-file-input" id="avatar">
							<label class="custom-file-label input_border_radius_right" for="avatar" name="avatar">
								Sélectionner une image
							</label>
						</div>
					</div>
					<div class="col-6 input-group">
						<div class="input-group-prepend">
							<span class="input-group-text input_border_radius_left">
								<i class="fas fa-align-left"></i>
							</span>
						</div>
						<textarea class="form-control input_border_radius_right" id="decouverte" name="decouverte" placeholder="mes messages"></textarea>
					</div>
				</div>
			</div>
			<button type="submit">Update</button>
		</form>
<script>
	activeNav('MonCompte');
</script>

<%@include file="include/foot.jsp"%>