package fr.eni.app.model.bll;

import java.util.HashMap;
import java.util.Map;

import fr.eni.app.dal.dao.IdentificationDAO;
import fr.eni.app.dal.dao.IdentificationDAOImpl;
import fr.eni.app.model.bll.bo.Utilisateur;

public class IdentificationManagerImpl implements IdentificationManager {

	private IdentificationDAO identificationDAO = IdentificationDAOImpl.getInstance();
	
	public static IdentificationManager instance = null;
	
	public IdentificationManagerImpl() {
		// TODO Auto-generated constructor stub
	}
	
	public static IdentificationManager getInstance() {
		if (instance == null) {
			instance = new IdentificationManagerImpl();
		}
		return instance;
	}
	
	@Override
	public Map<String, String> checkSignUp(String mdp, String mdpConfirm) {
		Map<String, String> errors = new HashMap<>();
		checkPasswords(mdp, mdpConfirm, errors);
		return errors;
	}

	@Override
	public void checkPasswords(String mdp, String mdpConfirm, Map<String, String> errors) {
		if(!mdp.equals(mdpConfirm)) {
			errors.put("confirmPassword", "Les mots de passe entrés sont différents");
		}
//		Pattern p = Pattern.compile("(([a-z].*[0-9])|([0-9].*[A-Z]))");
//		Matcher m = p.matcher(mdp);
//		if(!m.find()) {
//			errors.put("password", "Le mot de passe doit contenir au moins un chiffre et une lettre");
//		}
	}

	@Override
	public Utilisateur validSignup(String mail, String prenom, String nom, String mdp, String decouverte, String avatar) {
		boolean statut = false;
		// HASHAGE
		return identificationDAO.inscription(new Utilisateur(nom, prenom, mail, mdp, decouverte, avatar, statut));
	}

	@Override
	public Utilisateur getUser(String mail, String mdp) {
		System.out.println("getUser method");
		System.out.println(mail);
		Utilisateur utilisateur = identificationDAO.connexion(mdp,mail);
		return utilisateur;
	}
 // envoyer ds BDD (la)
	@Override
	public Utilisateur updateUtilisateur(int id, String nom, String prenom, String mdp, String mail, String decouverte,
			String avatar, boolean statut) {
		Utilisateur utilisateur = new Utilisateur(nom, prenom, mail, mdp, decouverte, avatar,statut );
		 return utilisateur;
	}
	
//	TODO
//	insertUser();
//	selectUser();
}
