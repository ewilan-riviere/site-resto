package fr.eni.app.model.bll;

import java.util.List;

import fr.eni.app.model.bll.bo.Plat;
import fr.eni.app.model.bll.bo.Utilisateur;

public interface PlatManager {

	Plat addPlat(String nom, String descr, String photo, String type, String prix, String recette);

	List<Plat> selectPlats();

}
