package fr.eni.app.model.bll.bo;

public class Utilisateur {
	private int id;
	private String nom;
	private String prenom;
	private String mdp;
	private String mail;
	private String decouverte;
	private boolean statut;
	private String avatar;
	
	
	
	public Utilisateur() {
		super();
	}
	
	


	public Utilisateur(String nom, String prenom, String mail, String mdp, String decouverte, String avatar, boolean statut) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.mdp = mdp;
		this.mail = mail;
		this.decouverte = decouverte;
		this.statut = statut;
		this.avatar = avatar;
	}




	public Utilisateur(int id, String nom, String prenom, String mail, String mdp, String decouverte, String avatar, boolean statut) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.mdp = mdp;
		this.mail = mail;
		this.decouverte = decouverte;
		this.statut = statut;
		this.avatar = avatar;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getPrenom() {
		return prenom;
	}



	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}



	public String getMdp() {
		return mdp;
	}



	public void setMdp(String mdp) {
		this.mdp = mdp;
	}



	public String getMail() {
		return mail;
	}



	public void setMail(String mail) {
		this.mail = mail;
	}



	public String getDecouverte() {
		return decouverte;
	}



	public void setDecouverte(String decouverte) {
		this.decouverte = decouverte;
	}



	public boolean isStatut() {
		return statut;
	}



	public void setStatut(boolean statut) {
		this.statut = statut;
	}



	public String getAvatar() {
		return avatar;
	}



	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}



	@Override
	public String toString() {
		return "Utilisateur [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", mdp=" + mdp + ", mail=" + mail
				+ ", decouverte=" + decouverte + ", statut=" + statut + ", avatar=" + avatar + "]";
	}
	
	
	
	
	
	
	
}
