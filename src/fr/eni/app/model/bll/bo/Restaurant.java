package fr.eni.app.model.bll.bo;

public class Restaurant {
	private int id;
	private String nom;
	private String geoloc;
	private String photo;
	private int nbtable;
	
	
	public Restaurant(int id, String nom, String geoloc, String photo, int nbtable) {
		super();
		this.id = id;
		this.nom = nom;
		this.geoloc = geoloc;
		this.photo = photo;
		this.nbtable = nbtable;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getGeoloc() {
		return geoloc;
	}


	public void setGeoloc(String geoloc) {
		this.geoloc = geoloc;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public int getNbtable() {
		return nbtable;
	}


	public void setNbtable(int nbtable) {
		this.nbtable = nbtable;
	}


	@Override
	public String toString() {
		return "Restaurant [id=" + id + ", nom=" + nom + ", geoloc=" + geoloc + ", photo=" + photo + ", nbtable="
				+ nbtable + "]";
	}
	
	
	

}
