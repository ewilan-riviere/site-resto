package fr.eni.app.model.bll;

import java.time.LocalDateTime;

import fr.eni.app.dal.dao.CommentairePlatDAO;
import fr.eni.app.dal.dao.CommentairePlatDAOImpl;
import fr.eni.app.model.bll.bo.Commentaire;

public class CommentairePlatManagerImpl implements CommentairePlatManager{
	private CommentairePlatDAO commentairePlatDAO = CommentairePlatDAOImpl.getInstance();
	
	public static CommentairePlatManager instance = null;
	
	public CommentairePlatManagerImpl() {
		// TODO Auto-generated constructor stub
	}
	
	public static CommentairePlatManager getInstance() {
		if (instance == null) {
			instance = new CommentairePlatManagerImpl();
		}
		return instance;
	}

	@Override
	public Commentaire newCommPlat(int idUtilisateur, int idPlat, String commentaire, float note) {
		Commentaire comm = new Commentaire(idUtilisateur, idPlat, commentaire, note, LocalDateTime.now());
		System.out.println("2" + comm);
		commentairePlatDAO.ajouteCommPlat(comm);
		return comm;
	}
}
