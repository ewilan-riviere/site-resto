package fr.eni.app.model.bll.bo.gender;

public enum Gender {
	
	Female("Femme"),
	Male("Homme"),
	Genderfluid("Genderfluid"),
	NonBinary("Non-binaire");
	
	private String gender;

	Gender(String gender) {
		this.gender = gender;
	}

	public String gender() {
		return gender;
	}

}
