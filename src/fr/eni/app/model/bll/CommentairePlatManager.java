package fr.eni.app.model.bll;

import fr.eni.app.model.bll.bo.Commentaire;

public interface CommentairePlatManager {

		public Commentaire newCommPlat(int idUtilisateur, int idPlat, String comm, float note);
}
