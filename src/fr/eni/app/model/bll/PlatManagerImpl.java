package fr.eni.app.model.bll;

import java.util.List;

import fr.eni.app.dal.dao.PlatDAO;
import fr.eni.app.dal.dao.PlatDAOImpl;
import fr.eni.app.model.bll.bo.Plat;
import fr.eni.app.model.bll.bo.TypePlat;

public class PlatManagerImpl implements PlatManager {

	private PlatDAO platDAO = PlatDAOImpl.getInstance();
	
	public static PlatManager instance = null;
	
	public PlatManagerImpl() {
		// TODO Auto-generated constructor stub
	}
	
	public static PlatManager getInstance() {
		if (instance == null) {
			instance = new PlatManagerImpl();
		}
		return instance;
	}

	@Override
	public Plat addPlat(String nom, String descr, String photo, String type, String prix, String recette) {
		System.out.println(type);
		int compteur = 0;
		TypePlat typeTyped = TypePlat.valueOf(type);
		float prixFloated = 0;
		if (!prix.isEmpty()) {
			prixFloated = Float.parseFloat(prix);
		}
		Plat plat = new Plat(nom, descr, photo, typeTyped, compteur, prixFloated, recette);
		boolean ok = platDAO.ajoutePlat(plat);
		if (ok) {
			return plat;
		} else {
			System.out.println("Error ajout de plat");
			return plat;
		}
	}

	@Override
	public List<Plat> selectPlats() {
		List<Plat> plats = platDAO.affichePlat();
		return plats;
	}
}
