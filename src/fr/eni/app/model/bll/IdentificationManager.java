package fr.eni.app.model.bll;

import java.util.Map;

import fr.eni.app.model.bll.bo.Utilisateur;

public interface IdentificationManager {

	void checkPasswords(String mdp, String mdpConfirm, Map<String, String> errors);

	Utilisateur validSignup(String mail, String prenom, String nom, String mdp, String decouverte, String avatar);

	Map<String, String> checkSignUp(String mdp, String mdpConfirm);

	Utilisateur getUser(String email, String password);

	 Utilisateur updateUtilisateur(int id, String nom, String prenom, String mdp, String mail, String decouverte, String avatar, boolean statut);

}
