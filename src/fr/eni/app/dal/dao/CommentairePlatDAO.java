package fr.eni.app.dal.dao;

import java.util.List;

import fr.eni.app.model.bll.bo.Commentaire;
import fr.eni.app.model.bll.bo.Plat;

public interface CommentairePlatDAO {
	public boolean ajouteCommPlat(Commentaire comm);
	public boolean supprimeCommPlat (Commentaire commentaire);
	public Commentaire modifieCommPlat(Commentaire commentaire);
	public List<Commentaire> afficheCommPlat(Plat plat);
}
