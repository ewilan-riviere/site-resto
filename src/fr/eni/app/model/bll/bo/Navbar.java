package fr.eni.app.model.bll.bo;

import java.io.Serializable;

public class Navbar {
	private static final long serialVersionUID = 1L;

	private String name;
	private String link;
	private String icon;
	
	public Navbar(String name, String link, String icon) {
		super();
		this.name = name;
		this.link = link;
		this.icon = icon;
	}
	

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}

	
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}

	@Override
	public String toString() {
		return "Navbar [name=" + name + ", link=" + link + ", icon=" + icon + "]";
	}
	
}
