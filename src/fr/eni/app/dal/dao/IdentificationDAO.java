package fr.eni.app.dal.dao;

import fr.eni.app.model.bll.bo.Utilisateur;

public interface IdentificationDAO {

	public Utilisateur inscription(Utilisateur utilisateur);
	public Utilisateur connexion(String mdp, String mail);
	public Utilisateur modification(Utilisateur utilisateur);
	
	//todo
	public void suppr(Utilisateur utilisateur);
}
