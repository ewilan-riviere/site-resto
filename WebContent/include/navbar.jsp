<nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light shadow-sm mb-5 bg-white rounded">
	<a class="navbar-brand" href="${ pageContext.request.contextPath }/home">
		<img class="d-inline-block align-top" width="30" height="30" src="${ pageContext.request.contextPath }/assets/images/logo.png"> 
		<span class="navbar_title">
			<fmt:message key="head.title.title" bundle="${ title }"/>
		</span>
	</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTemplate" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	  <span class="navbar-toggler-icon"></span>
	</button>

  	<div class="collapse navbar-collapse" id="navbarTemplate">
    	<ul class="navbar-nav mr-auto">
    
			<c:forEach items="${ NavbarMain }" var="NavbarMainItems">
				<li id="${ NavbarMainItems.name }" class="nav-item">
				  <a class="nav-link" href="${ pageContext.request.contextPath }/${ NavbarMainItems.link }">
					<i class="${ NavbarMainItems.icon }"></i> 
					<fmt:message key="navbar.main.${ NavbarMainItems.name }" bundle="${ string }"/>
				  </a>
				</li>
			</c:forEach>
			<c:choose>
				<%-- If User is not connected --%>
				<c:when test="${ isConnected == false }">
					<c:forEach items="${ navbarNoConnected }" var="navbarNoConnectedItems">
						<li id="${ navbarNoConnectedItems.name }" class="nav-item">
						  <a class="nav-link" href="${ pageContext.request.contextPath }/${ navbarNoConnectedItems.link }">
							<i class="${ navbarNoConnectedItems.icon }"></i> 
							<fmt:message key="navbar.main.${ navbarNoConnectedItems.name }" bundle="${ string }"/>
						  </a>
						</li>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<c:forEach items="${ navbarConnected }" var="navbarConnectedItems">
						<li id="${ navbarConnectedItems.name }" class="nav-item">
						  <a class="nav-link" href="${ pageContext.request.contextPath }/${ navbarConnectedItems.link }">
							<i class="${ navbarConnectedItems.icon }"></i> 
							<fmt:message key="navbar.main.${ navbarConnectedItems.name }" bundle="${ string }"/>
						  </a>
						</li>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</ul>
	</div>
</nav>