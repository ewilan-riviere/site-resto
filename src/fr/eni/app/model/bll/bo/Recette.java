package fr.eni.app.model.bll.bo;

public class Recette {
	private int id;
	private int idIgredient;
	private int idPlat;
	private int quantite;
	
	
	public Recette(int id, int idIgredient, int idPlat, int quantite) {
		super();
		this.id = id;
		this.idIgredient = idIgredient;
		this.idPlat = idPlat;
		this.quantite = quantite;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getIdIgredient() {
		return idIgredient;
	}


	public void setIdIgredient(int idIgredient) {
		this.idIgredient = idIgredient;
	}


	public int getIdPlat() {
		return idPlat;
	}


	public void setIdPlat(int idPlat) {
		this.idPlat = idPlat;
	}


	public int getQuantite() {
		return quantite;
	}


	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}


	@Override
	public String toString() {
		return "Recette [id=" + id + ", idIgredient=" + idIgredient + ", idPlat=" + idPlat + ", quantite=" + quantite
				+ "]";
	}
	
	
	
	
	

}
