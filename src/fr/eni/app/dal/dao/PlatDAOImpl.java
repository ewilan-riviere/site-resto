package fr.eni.app.dal.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.eni.app.dal.PoolConnection;
import fr.eni.app.model.bll.bo.Plat;
import fr.eni.app.model.bll.bo.TypePlat;

public class PlatDAOImpl implements PlatDAO{
	public static PlatDAO instance = null;
	
	public PlatDAOImpl() {
		// TODO Auto-generated constructor stub
	}
	
	public static PlatDAO getInstance() {
		if (instance == null) {
			instance = new PlatDAOImpl();
		}
		return instance;
	}

	@Override
	public boolean ajoutePlat(Plat plat) {
		Connection cnx = PoolConnection.getConnection();
		boolean ok = false;
		try {
			if (plat.getPhoto() == "") plat.setPhoto("default");
			
			PreparedStatement query = cnx.prepareStatement("INSERT INTO Plat(nom, descr, photo, type, prix, recette) VALUES (?,?,?,?,?,?)");
			query.setString(1, plat.getNom());
			query.setString(2, plat.getDescr());
			query.setString(3, plat.getPhoto());
			query.setString(4, plat.getTypePlat().toString());
			query.setFloat(5, plat.getPrix());
			query.setString(6, plat.getRecette());
			
			
			query.executeUpdate();
			
			cnx.close();
			
		}catch(SQLException e) {System.out.println(e.toString());}	
		return ok;
	}

	@Override
	public void supprimePlat(Plat plat) {
		Connection cnx = PoolConnection.getConnection();
		try {
			PreparedStatement query = cnx.prepareStatement("DELETE FROM Plat WHERE id=?");
			query.setInt(1, plat.getId());
			
			query.executeUpdate();
			
			cnx.close();
		}catch(SQLException e) {e.toString();}
	}

	@Override
	public Plat modifiePlat(Plat plat) {
		Connection cnx = PoolConnection.getConnection();
		try {
			PreparedStatement query = cnx.prepareStatement("UPDATE Utilisateur SET nom = ?, descr = ?, photo = ?, type = ?, prix = ?, recette = ?) WHERE id=?");
			query.setString(1, plat.getNom());
			query.setString(2, plat.getDescr());
			query.setString(3, plat.getPhoto());
			query.setString(4, plat.getTypePlat().toString());
			query.setFloat(5, plat.getPrix());
			query.setString(6, plat.getRecette());
			query.setInt(7, plat.getId());
			
			query.executeUpdate();
			
			cnx.close();
		}catch(SQLException e) {e.toString();}
		return plat;	
	}

	@Override
	public List<Plat> affichePlat() {
		List<Plat> plats = new ArrayList<Plat>();
		Connection cnx = PoolConnection.getConnection();
		try {
			PreparedStatement pstmt = cnx.prepareStatement("SELECT * FROM Plat");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Plat plat = new Plat();
				
				plat.setId(rs.getInt("id"));
				plat.setNom(rs.getString("nom"));
				plat.setDescr(rs.getString("descr"));
				plat.setPhoto(rs.getString("photo"));
				plat.setTypePlat(TypePlat.valueOf((rs.getString("type"))));
				plat.setCompteur(rs.getInt("compteur"));
				plat.setPrix(rs.getFloat("prix"));
				plat.setRecette(rs.getString("recette"));
				
				plats.add(plat);
			}
			cnx.close();
		}catch(SQLException e) {System.out.println(e.toString());}
		return plats;
	}
	
}
